#ifndef TREE_H
#define TREE_H

#include <list>

template <class v_type>
class Tree
{
    public:
        Tree();
        ~Tree();

    std::list< Tree<v_type> >& get_children(void){
        return children;
    };
    protected:

    private:
    v_type* value;
    std::list< Tree<v_type>> children;
};

#endif // TREE_H
