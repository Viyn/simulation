#ifndef SPHERE_H
#define SPHERE_H

#include <Body.h>
#include <cmath>


class Sphere : public Body
{
    public:
        Sphere();
        ~Sphere();

        double calc_collision(Sphere& b){
        arma::Row<double> r = b.pos - pos;
        double l = sqrt(arma::sum(r%r));
            return std::max(R+b.R-l,0.0);
        };
    protected:
    double R;

    private:
};

#endif // SPHERE_H
