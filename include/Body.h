#ifndef BODY_H
#define BODY_H

#include <armadillo>


using namespace arma;


class Body
{
public:
    Body();
    virtual ~Body();

protected:
    double inert;
    mat inert_m;

    Col<double> pos;
    Col<double> vel;
    Col<double> acc;

    Col<double> rot;
    Col<double> ang_vel;
};

#endif // BODY_H
