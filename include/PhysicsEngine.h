#ifndef PHYSICSENGINE_H
#define PHYSICSENGINE_H

#include "Tree.h"
#include "Body.h"

class PhysicsEngine
{
    public:
        PhysicsEngine();
        ~PhysicsEngine();

        void step(void);


    protected:
        void calc_movoment(void);
        void find_collisions(void);
        void resolve_collisions(void);

    std::list< Tree<Body> > body_tree_list;
};

#endif // PHYSICSENGINE_H
